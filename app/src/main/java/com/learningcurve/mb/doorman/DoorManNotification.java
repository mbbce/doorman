package com.learningcurve.mb.doorman;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.learningcurve.mb.doorman.R;

public class DoorManNotification {

    public static int notifId = 654655;

    public static Notification getNotification(Context context) {

        Notification n = new Notification(
                R.drawable.small_icon,
                "DoorMan Service!",
                System.currentTimeMillis());

        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                0,
                new Intent(context, MainActivity.class),
                0);
        n.setLatestEventInfo(context, "DoorMan: ", "Tagging your traffic!", pendingIntent);

        return n;
    }

    public static void cancel(Context context) {
        NotificationManager nm =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        nm.cancel(notifId);
    }

}
