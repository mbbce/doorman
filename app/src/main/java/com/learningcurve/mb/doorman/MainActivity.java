package com.learningcurve.mb.doorman;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Environment;
import android.os.IBinder;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.learningcurve.mb.doorman.R;

import java.io.File;


public class MainActivity extends Activity {
    Intent serviceIntent;
    ToggleButton startStop;
    ToggleButton disconnect;
    Button refreshButton;
    Button delButton;
    TextView size;
    ImageView icon;

    SharedPreferences sh;
    File folder = new File(Environment.getExternalStorageDirectory()+ File.separator+"DoorMan-Logs");

    @Override
    public void onResume(){
        super.onResume();
        sh = getSharedPreferences("gkpref", MODE_PRIVATE);
        startStop.setChecked(sh.getBoolean("butState", false));
        if(sh.getBoolean("butState",false)){
            icon.setImageResource(R.drawable.door_open);
        }else{
            icon.setImageResource(R.drawable.door_closed);
        }
        disconnect.setChecked(sh.getBoolean("disconnect",false));
        //Log.v("Debug GateKeeper:","onResume called"+startStop.isChecked());
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Boolean state = false;
        Bundle b = getIntent().getExtras();
        if(b!=null) {
            Log.v(DoorManService.TAG,"Getting boolean");
           state = b.getBoolean("Button", false);
        }
        setContentView(R.layout.activity_main);
        sh = getSharedPreferences("gkpref", MODE_PRIVATE);
        startStop = (ToggleButton)findViewById(R.id.startStop);
        disconnect = (ToggleButton)findViewById(R.id.disconnectButton);
        refreshButton = (Button) findViewById(R.id.refreshButton);
        size = (TextView) findViewById(R.id.logspace);
        icon = (ImageView) findViewById(R.id.imageView);
        delButton = (Button) findViewById(R.id.delButton);

        icon.setImageResource(R.drawable.door_closed);

        startStop.setChecked(sh.getBoolean("butState", false));
        disconnect.setChecked(sh.getBoolean("disconnect", false));
        Log.v(DoorManService.TAG, "Activity Started");
        startStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startStop.isChecked()) {
                    serviceIntent = new Intent(getApplicationContext(),
                            DoorManService.class);
                    //serviceIntent.putExtra("Button", (Parcelable) startStop);
                    startService(serviceIntent);
                    Log.v("GateKeeper: ", "Starting service");
                    bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
                    SharedPreferences.Editor editor = sh.edit();
                    editor.putBoolean("butState", startStop.isChecked());
                    editor.commit();
                    icon.setImageResource(R.drawable.door_open);
                } else {
                    SharedPreferences.Editor editor = sh.edit();
                    editor.putBoolean("butState", startStop.isChecked());
                    editor.commit();
                    Log.v(DoorManService.TAG, "Stopping service");
                    if (serviceIntent != null) {
                        stopService(serviceIntent);
                    }
                    DoorManService.stop();
                    icon.setImageResource(R.drawable.door_closed);
                }
            }
        });

        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sh.edit();
                editor.putBoolean("disconnect", disconnect.isChecked());
                editor.commit();
            }
        });

        //Click the button
        if(state==true){
            Log.v(DoorManService.TAG,"Clicking the button");
            if(!startStop.isChecked())
                startStop.performClick();
        }

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(folder.exists()) {
                    size.setText(String.valueOf(folderSize(folder))+" KB");
                }else{
                    folder.mkdir();
                    size.setText(String.valueOf(0.0) + " KB");
                }

            }
        });

        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(folder.exists() && isOnline()) {
                    Toast t =Toast.makeText(getApplicationContext(),
                            "Deleting the folder",Toast.LENGTH_SHORT);
                    t.show();

                    for(File f :folder.listFiles()){
                        AsyncUpload task = new AsyncUpload(f);
                        task.execute();
                    }
                    //folder.delete();
                }else if(isOnline()){
                    Toast t =Toast.makeText(getApplicationContext(),
                            "Folder does not exist",Toast.LENGTH_SHORT);
                    t.show();
                }else{
                    Toast t =Toast.makeText(getApplicationContext(),
                            "Please connect to internet and press delete again",Toast.LENGTH_SHORT);
                    t.show();
                }
            }
        });

    }

    public static float folderSize(File directory) {
        long length = 0;
        for (File file : directory.listFiles()) {
            if (file.isFile())
                length += file.length();
            else
                length += folderSize(file);
        }

        float len = length/(1024);
        Log.v(DoorManService.TAG,""+len);
        return len;
    }
    /*
    protected void onClick(){
        if(startStop.isChecked()){
            serviceIntent = new Intent(getApplicationContext(),
                    DoorManService.class);
            startService(serviceIntent);
            Log.v("GateKeeper: ", "Starting service");
            bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        }else{
            Log.v("GateKeeper: ", "Stopping service");
            stopService(serviceIntent);
        }
    }*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            //updateHandler.removeCallbacks(updater); // removes post delayeds
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            DoorManService.MonitorLocalBinder binder = (DoorManService.MonitorLocalBinder) service;
            //DoorManService.stop(binder.getService());

        }
    };

    public Boolean isOnline() {

        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal==0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;

        //return true;
    }

}

