package com.learningcurve.mb.doorman;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;
import java.io.File;

/**
 * Created by mb on 28/05/15.
 */
public class ChargingReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Check for the internet state and the screen state. When
        while(true) {
            Log.v(DoorManService.TAG, "Charging .....");
            //TOFIX: The screen state check is hanging the phone
            if (isOnline()) {
                Toast t = Toast.makeText(context,
                        "Internet available", Toast.LENGTH_SHORT);
                t.show();
                File folder = DoorManThread.folder;
                for (File f : folder.listFiles()) {
                    if (f.isFile() && f.exists()) {
                        AsyncUpload task = new AsyncUpload(f);
                        task.execute();
                    } else {
                        Log.v(DoorManService.TAG, "File does not exist");
                    }
                }
                break;

            } else {
                Toast t = Toast.makeText(context,
                        "Internet unavailable", Toast.LENGTH_SHORT);
                t.show();
            }
            // Sleep
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Boolean isScreenOn(Context context){
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        return pm.isScreenOn();
    }
    public Boolean isOnline() {

        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal==0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;

        //return true;
    }

}
