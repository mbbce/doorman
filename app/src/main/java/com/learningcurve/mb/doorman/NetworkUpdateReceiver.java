package com.learningcurve.mb.doorman;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

/**
 * Created by mb on 8/05/15.
 */
public class NetworkUpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        /*
        if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals("android.net.wifi.STATE_CHANGE")) {
            NetworkInfo netInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if (ConnectivityManager.TYPE_WIFI == netInfo.getType()) {
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo();
                Log.v(DoorManService.TAG, "SSID is: " + info.getBSSID());
            }
        }else{
            Log.v(DoorManService.TAG, "State: " + WifiManager.NETWORK_STATE_CHANGED_ACTION);
        }*/



        boolean isConnected = isRelevantConnection(context);
        Log.v(DoorManService.TAG,"Update Receive");
        if (isConnected) {
            if(!DoorManService.isRunning()&&DoorManService.exists()) {
                Toast t = Toast.makeText(context,
                        "Restarting ....", Toast.LENGTH_SHORT);
                t.show();

                //Restart the service if it was paused previously
                DoorManService.restart();
            }else if(!DoorManService.exists()){
                //Verify the functionality of this piece of code
                Intent i = new Intent(context,MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle b = new Bundle();
                b.putBoolean("Button", true);
                i.putExtras(b);
                Log.v(DoorManService.TAG,"Starting Activity");
                context.startActivity(i);
            }
            else{
                Log.v(DoorManService.TAG,"Unncessary call, doing nothing");
            }
        }
        else{
            Toast t =Toast.makeText(context,
                    "Pausing ....", Toast.LENGTH_SHORT);
            t.show();
            DoorManService.pause();

        }
    }

    public static boolean isRelevantConnection(Context serviceContext) {
        ConnectivityManager cm = (ConnectivityManager)
                serviceContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        if (ni == null || !ni.isConnected()) {

            if(ni==null)
                Log.v(DoorManService.TAG, "Network info is null");
            else{
                Log.v(DoorManService.TAG, "Network is not connected");
            }
            // There are no active networks.
            return false;
        } else{
            WifiManager wifiManager = (WifiManager) serviceContext.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            String ssid = wifiInfo.getSSID();
            Log.v(DoorManService.TAG, "SSID: " + ssid);
            if(ssid.equals("\"InspectorTraffic\"")){
                return true;
            }
            else {
                Boolean disconnect = false;
                SharedPreferences sh = serviceContext.getSharedPreferences("gkpref",Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = sh.edit();
                disconnect = sh.getBoolean("disconnect",false);
                if(disconnect){
                    Log.v(DoorManService.TAG,"Stop connecting to that network");
                    return false;
                }else{
                    return connect(serviceContext);
                }
            }
        }

    }

    public static Boolean connect(Context context){
        String networkSSID = "InspectorTraffic";
        String networkPass = "researchnet";

        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + networkSSID + "\"";   // Please note the quotes. String should contain ssid in quotes
        conf.preSharedKey = "\""+ networkPass +"\"";
        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);

        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        Boolean found = false;
        int networkId = 0;

        for(WifiConfiguration net: list){
            if(net.SSID != null && net.SSID.equals("\"" + networkSSID + "\"")) {
                networkId = net.networkId;
                found = true;
                break;
            }
        }
        if(!found) {
            networkId = wifiManager.addNetwork(conf);
        }
        Log.v(DoorManService.TAG,"NetworkID "+networkId);

        List<ScanResult> listScan = wifiManager.getScanResults();
        for(ScanResult i : listScan){
            Log.v(DoorManService.TAG,""+i.SSID);
            if(i.SSID != null && i.SSID.equals(networkSSID)) {
                //wifiManager.disconnect();
                //wifiManager.enableNetwork(i.networkId, true);
                //wifiManager.reconnect();
                Log.v(DoorManService.TAG,"Match is happening");
                /*
                ComponentName receiver = new ComponentName(context, NetworkUpdateReceiver.class);

                PackageManager pm = context.getPackageManager();

                pm.setComponentEnabledSetting(receiver,
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                        PackageManager.DONT_KILL_APP);
                        */
                for( WifiConfiguration net : list ) {
                    if(net.SSID != null && net.SSID.equals("\"" + networkSSID + "\"")) {
                        Log.v(DoorManService.TAG, "" + net.SSID);
                        wifiManager.disconnect();
                        //wifiManager.enableNetwork(i.networkId, true);
                        //wifiManager.reconnect();
                        if(wifiManager.enableNetwork(net.networkId, true)){
                            Log.v(DoorManService.TAG,"Connection successful");
                            /*
                            pm.setComponentEnabledSetting(receiver,
                                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                                    PackageManager.DONT_KILL_APP);
                                    */
                            return true;
                        }else{
                            Log.v(DoorManService.TAG, "Connection unsuccessful");
                            return false;
                        }
                    }
                }

            }
        }
        return false;
    }

}
