package com.learningcurve.mb.doorman;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.ToggleButton;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by mb on 21/05/15.
 */
public class DoorManService extends Service {
    private final IBinder binder = new MonitorLocalBinder();
    public static final String TAG = "DoorMan: ";
    //static PathFileObserver gkt;
    static DoorManThread gkt;
    static Thread t;
    static DoorManService itself;
    public class MonitorLocalBinder extends Binder {
        DoorManService getService() {
            return DoorManService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "onStartCommand");
        //RootTools.debugMode = true; // ON
        itself = DoorManService.this;
        try {
            run();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		/*
		 * -START_STICKY is used for services that are explicitly started and
		 * stopped as needed. -START_NOT_STICKY or START_REDELIVER_INTENT are
		 * used for services that should only remain running while processing
		 * any commands sent to them.
		 */
        return (START_STICKY);
    }

    public static void pause(){
        Log.v(DoorManService.TAG, "Excecuting pause");
        //gkt.stopWatching();
        //Just make sure that the gkt is not null
        //Since the wifi state can change while the application is not turned on
        if(gkt!=null) {
            gkt.pauseWork();
        }
    }

    public static void restart(){
        Log.v(DoorManService.TAG, "Excecuting restart");
        //gkt.stopWatching();

        if(gkt!=null) {
            gkt.restartWork();
        }
    }

    public static void stop(){
        Log.v(DoorManService.TAG, "Excecuting stop");
        //gkt.stopWatching();
        if(gkt!=null) {
            gkt.stopWork();
        }
        stop(itself);
        //t.stop();
        //t.destroy();
    }
    public static void stop(Service service){
        Log.v(DoorManService.TAG, "Executing stop Foreground");
        service.stopForeground(true);
    }

    public static boolean isRunning(){
        if(gkt!=null) {
            return gkt.isRunning();
        }
        return false;
    }
    public static boolean exists(){
        if(gkt!=null) {
            return true;
        }else {
            return false;
        }
    }
    @Override
    public void onDestroy(){


    }
    public void run() throws InterruptedException, IOException {
        gkt  = new DoorManThread(getPackageManager(),getApplicationContext());
        t = new Thread(gkt);
        t.start();

        /*
        Log.v(DoorManService.TAG, "Starting the observation");
        File f = new File("/proc/net/");
        Log.v(DoorManService.TAG,f.getAbsolutePath());
        gkt = new  PathFileObserver(f.getAbsolutePath());
        gkt.startWatching();
        */
        startForeground(
                DoorManNotification.notifId,
                DoorManNotification
                        .getNotification(getApplicationContext()));
        //gkt.run();
    }

}
