package com.learningcurve.mb.doorman;

import android.os.FileObserver;
import android.util.Log;
import java.io.File;


public class PathFileObserver extends FileObserver {
    static final String TAG="FILEOBSERVER";
    /**
     * should be end with File.separator
     */
    String rootPath;
    static final int mask = (FileObserver.CREATE |
            FileObserver.DELETE |
            FileObserver.DELETE_SELF |
            FileObserver.MODIFY |
            FileObserver.MOVED_FROM |
            FileObserver.MOVED_TO |
            FileObserver.MOVE_SELF);

    public PathFileObserver(String root){
        super(root, mask);

        if (! root.endsWith(File.separator)){
            root += File.separator;
        }
        rootPath = root;
    }

    public void onEvent(int event, String path) {
        Log.v(DoorManService.TAG,"onEvent");
        switch(event){
            case FileObserver.CREATE:
                Log.d(TAG, "CREATE:" + rootPath + path);
                break;
            case FileObserver.DELETE:
                Log.d(TAG, "DELETE:" + rootPath + path);
                break;
            case FileObserver.DELETE_SELF:
                Log.d(TAG, "DELETE_SELF:" + rootPath + path);
                break;
            case FileObserver.MODIFY:
                Log.d(TAG, "MODIFY:" + rootPath + path);
                break;
            case FileObserver.MOVED_FROM:
                Log.d(TAG, "MOVED_FROM:" + rootPath + path);
                break;
            case FileObserver.MOVED_TO:
                Log.d(TAG, "MOVED_TO:" + path);
                break;
            case FileObserver.MOVE_SELF:
                Log.d(TAG, "MOVE_SELF:" + path);
                break;
            default:
                // just ignore
                break;
        }
    }

    public void close(){
        super.finalize();
    }
}