package com.learningcurve.mb.doorman;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.TrafficStats;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mb on 21/05/15.
 */
public class DoorManThread implements Runnable {
    private static PackageManager pm;
    private static Context ctx;
    private boolean work = true;
    private boolean pause = false;
    public static File folder = new File(Environment.getExternalStorageDirectory()+ File.separator+"DoorMan-Logs");;
    public DoorManThread(PackageManager pkgm, Context c){
        pm = pkgm;
        ctx = c;
    }

    public void stopWork(){
        work = false;
    }

    public boolean isRunning(){
        return !pause;
    }

    public void pauseWork(){
        pause = true;
    }

    public void restartWork(){
        pause = false;
    }
    @Override
    public void run() {

        //get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(
                PackageManager.GET_META_DATA);
        int UID;
        //loop through the list of installed packages and see if the selected
        //app is in the list
        Log.v("GateKeeper: ", "Check packages");
        //Log.v("GateKeeper: ",""+packages);
        for (ApplicationInfo packageInfo : packages) {
            /*if(packageInfo.packageName.equals(app_selected)){
                //get the UID for the selected app
                UID = packageInfo.uid;
                break; //found a match, don't need to search anymore
            }*/
            //Log.v("GateKeeper:",""+packageInfo.uid);
            Log.v("GateKeeper: ", "Package: "+packageInfo.packageName+" UID is: " + packageInfo.uid);

        }

        if(!folder.exists()){
            folder.mkdir();
        }

        HashMap<String,LinkedList<String>> appConn = new HashMap<String,LinkedList<String>>();
        long newTotalTx = TrafficStats.getTotalTxPackets();
        long newTotalRx = TrafficStats.getTotalRxPackets();
        long prevTotalTx = 0;
        long prevTotalRx = 0;
        Calendar c = Calendar.getInstance();
        Date d = c.getTime();
        long min = d.getTime();
        while(work){
            if((newTotalTx-prevTotalTx>1)||(newTotalRx-prevTotalRx>1)&&!pause) {
                //Log.v("GateKeeper: ","New packet, working");
                BufferedReader bufferedReader = null;
                try {
                    bufferedReader = new BufferedReader(new FileReader("/proc/net/tcp6"));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                String line;
                //Log.v("GateKeeper: ", "Reading file");
                try {
                    bufferedReader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    while ((line = bufferedReader.readLine()) != null) {

                        line = line.trim();
                        String[] fields = line.split("\\s+", 10);
                        int fieldn = 0;

                        String src[] = fields[1].split(":", 2);
                        String dst[] = fields[2].split(":", 2);
                        String srcip = getAddress6(src[0]);
                        String spt = String.valueOf(getInt16(src[1]));
                        String dstip = getAddress6(dst[0]);
                        String dpt = String.valueOf(getInt16(dst[1]));
                        String uid = fields[7];

                        String packageName = findPackageName(packages, uid);
                        String tuple = srcip + "," + dstip + "," + spt + "," + dpt;
                        if (appConn.containsKey(packageName)) {
                            LinkedList<String> tupleList = appConn.get(packageName);
                            if (!tupleList.contains(tuple)) {
                                tupleList.add(tuple);
                                appConn.put(packageName, tupleList);
                                //Log.v("GateKeeper: ", line);
                            }
                        } else {
                            appConn.put(packageName, new LinkedList<String>(Arrays.asList(tuple)));
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //Log.v("GateKeeper: ", "Size of map " + appConn.size());
                prevTotalRx = newTotalRx;
                prevTotalTx = newTotalTx;
                newTotalTx = TrafficStats.getTotalTxPackets();
                newTotalRx = TrafficStats.getTotalRxPackets();
                //Log.v(DoorManService.TAG,""+newTotalTx+" "+prevTotalTx);
            }else{
                try {
                    //Log.v("GateKeeper: ","No new packet sleeping");
                    Thread.sleep(500);
                    newTotalTx = TrafficStats.getTotalTxPackets();
                    newTotalRx = TrafficStats.getTotalRxPackets();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            //Dump all the log in a file are empty out the data structure
            c = Calendar.getInstance();
            d = c.getTime();
            //Log.v("Time ",""+d.getTime());
            long current = d.getTime();
            if(current-min>=600000) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String fileName = sdf.format(d);
                fileName = fileName.replace(" ","_");
                fileName = fileName.replace(":","-");

                File f = new File(folder.getAbsolutePath()+File.separator + fileName+".log");
                try {
                    Log.v(DoorManService.TAG,"Writing to file "+f.getAbsolutePath());
                    BufferedWriter bf = new BufferedWriter(new FileWriter(f));
                    for(String pkName : appConn.keySet()){
                        LinkedList<String> tupleList = appConn.get(pkName);
                        for(int i=0;i<tupleList.size();i++){
                            bf.write(pkName+","+tupleList.get(i)+"\n");
                        }
                    }
                    bf.close();
                    appConn = new HashMap<String,LinkedList<String>>();
                    min = current;
                    //UploadLogs.uploadFile(f);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //Log.v("Gate Keeper: ",""+appConn);
    }

    private String findPackageName(List<ApplicationInfo> packages, String uid){
        for (ApplicationInfo packageInfo : packages) {
            if(packageInfo.uid==Integer.parseInt(uid)){
                return packageInfo.packageName;
            }
        }
        return null;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private final String getAddress(final String hexa) {
        try {
            final long v = Long.parseLong(hexa, 16);
            final long adr = (v >>> 24) | (v << 24) |
                    ((v << 8) & 0x00FF0000) | ((v >> 8) & 0x0000FF00);
            return ((adr >> 24) & 0xff) + "." + ((adr >> 16) & 0xff) + "." + ((adr >> 8) & 0xff) + "." + (adr & 0xff);
        } catch(Exception e) {
            //Log.w("NetworkLog", e.toString(), e);
            return "-1.-1.-1.-1";
        }
    }

    private final String getAddress6(final String hexa) {
        try {
            final String ip4[] = hexa.split("0000000000000000FFFF0000");

            if(ip4.length == 2) {
                final long v = Long.parseLong(ip4[1], 16);
                final long adr = (v >>> 24) | (v << 24) |
                        ((v << 8) & 0x00FF0000) | ((v >> 8) & 0x0000FF00);
                return ((adr >> 24) & 0xff) + "." + ((adr >> 16) & 0xff) + "." + ((adr >> 8) & 0xff) + "." + (adr & 0xff);
            } else {
                return "-2.-2.-2.-2";
            }
        } catch(Exception e) {
            //Log.w("NetworkLog", e.toString(), e);
            return "-1.-1.-1.-1";
        }
    }

    private final int getInt16(final String hexa) {
        try {
            return Integer.parseInt(hexa, 16);
        } catch(Exception e) {
            //Log.w("NetworkLog", e.toString(), e);
            return -1;
        }
    }
}
