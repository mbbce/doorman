package com.learningcurve.mb.doorman;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.FileObserver;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mb on 21/05/15.
 */
public class NewGateKeeperThread extends FileObserver implements Runnable {
    private static PackageManager pm;
    String rootPath;
    private boolean work = true;

    public NewGateKeeperThread(PackageManager pkgm,String root){

        super(root, PathFileObserver.mask);

        if (! root.endsWith(File.separator)){
            root += File.separator;
        }
        rootPath = root;
        pm = pkgm;
    }

    public void onEvent(int event, String path) {

        switch(event){
            case FileObserver.CREATE:
                Log.d(DoorManService.TAG, "CREATE:" + rootPath + path);
                break;
            case FileObserver.DELETE:
                Log.d(DoorManService.TAG, "DELETE:" + rootPath + path);
                break;
            case FileObserver.DELETE_SELF:
                Log.d(DoorManService.TAG, "DELETE_SELF:" + rootPath + path);
                break;
            case FileObserver.MODIFY:
                Log.d(DoorManService.TAG, "MODIFY:" + rootPath + path);
                break;
            case FileObserver.MOVED_FROM:
                Log.d(DoorManService.TAG, "MOVED_FROM:" + rootPath + path);
                break;
            case FileObserver.MOVED_TO:
                Log.d(DoorManService.TAG, "MOVED_TO:" + path);
                break;
            case FileObserver.MOVE_SELF:
                Log.d(DoorManService.TAG, "MOVE_SELF:" + path);
                break;
            default:
                // just ignore
                break;
        }
    }
    public void stopWork(){
        work = false;
    }

    @Override
    public void run() {

        //get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(
                PackageManager.GET_META_DATA);
        int UID;
        //loop through the list of installed packages and see if the selected
        //app is in the list
        Log.v("GateKeeper: ", "Check packages");
        //Log.v("GateKeeper: ",""+packages);
        for (ApplicationInfo packageInfo : packages) {
            /*if(packageInfo.packageName.equals(app_selected)){
                //get the UID for the selected app
                UID = packageInfo.uid;
                break; //found a match, don't need to search anymore
            }*/
            //Log.v("GateKeeper:",""+packageInfo.uid);
            Log.v("GateKeeper: ", "Package: "+packageInfo.packageName+" UID is: " + packageInfo.uid);

        }
        HashMap<String,LinkedList<String>> appConn = new HashMap<String,LinkedList<String>>();
        while(work){
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new FileReader("/proc/net/tcp6"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String line;
            //Log.v("GateKeeper: ", "Reading file");
            try {
                bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                while(( line= bufferedReader.readLine()) !=null){

                    line = line.trim();
                    String[] fields = line.split("\\s+", 10);
                    int fieldn = 0;
                    String src[] = fields[1].split(":", 2);
                    String dst[] = fields[2].split(":", 2);
                    String srcip = getAddress6(src[0]);
                    //Log.v("GateKeeper: "," tcp Netstat: connection.src " + srcip);
                    String spt = String.valueOf(getInt16(src[1]));
                    //Log.v("GateKeeper: "," tcp Netstat: connection.spt " + spt);
                    String dstip = getAddress6(dst[0]);
                    //Log.v("GateKeeper: "," tcp Netstat: connection.dst " + dstip);
                    String dpt = String.valueOf(getInt16(dst[1]));
                    //Log.v("GateKeeper: "," tcp Netstat: connection.dpt " + dpt);
                    String uid = fields[7];
                    //Log.v("GateKeeper: "," tcp Netstat: connection.uid " + uid);
                    String packageName  =findPackageName(packages,uid);
                    //Log.v("GateKeeper: ", " Package Name: " + packageName);
                    String tuple = srcip + "," + dstip + "," + spt + "," + dpt;
                    if(appConn.containsKey(packageName)) {
                        LinkedList<String> tupleList = appConn.get(packageName);
                        if(!tupleList.contains(tuple)) {
                            tupleList.add(tuple);
                            appConn.put(packageName, tupleList);
                            Log.v("GateKeeper: ", line);
                        }
                    }
                    else{
                        appConn.put(packageName, new LinkedList<String>(Arrays.asList(tuple)));
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("GateKeeper: ","Size of map "+appConn.size());
            Log.v("GateKeeper: ",""+appConn);
        }
        Log.v("Gate Keeper: ",""+appConn);
    }

    private String findPackageName(List<ApplicationInfo> packages, String uid){
        for (ApplicationInfo packageInfo : packages) {
            if(packageInfo.uid==Integer.parseInt(uid)){
                return packageInfo.packageName;
            }
        }
        return null;
    }
    private final String getAddress(final String hexa) {
        try {
            final long v = Long.parseLong(hexa, 16);
            final long adr = (v >>> 24) | (v << 24) |
                    ((v << 8) & 0x00FF0000) | ((v >> 8) & 0x0000FF00);
            return ((adr >> 24) & 0xff) + "." + ((adr >> 16) & 0xff) + "." + ((adr >> 8) & 0xff) + "." + (adr & 0xff);
        } catch(Exception e) {
            Log.w("NetworkLog", e.toString(), e);
            return "-1.-1.-1.-1";
        }
    }

    private final String getAddress6(final String hexa) {
        try {
            final String ip4[] = hexa.split("0000000000000000FFFF0000");

            if(ip4.length == 2) {
                final long v = Long.parseLong(ip4[1], 16);
                final long adr = (v >>> 24) | (v << 24) |
                        ((v << 8) & 0x00FF0000) | ((v >> 8) & 0x0000FF00);
                return ((adr >> 24) & 0xff) + "." + ((adr >> 16) & 0xff) + "." + ((adr >> 8) & 0xff) + "." + (adr & 0xff);
            } else {
                return "-2.-2.-2.-2";
            }
        } catch(Exception e) {
            Log.w("NetworkLog", e.toString(), e);
            return "-1.-1.-1.-1";
        }
    }

    private final int getInt16(final String hexa) {
        try {
            return Integer.parseInt(hexa, 16);
        } catch(Exception e) {
            Log.w("NetworkLog", e.toString(), e);
            return -1;
        }
    }
}
