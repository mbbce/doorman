package com.learningcurve.mb.doorman;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by mb on 11/06/15.
 */
public class AsyncUpload extends AsyncTask{
    private File file;

    public AsyncUpload(File f) {
        file = f;
    }
    @Override
    protected Object doInBackground(Object[] params) {
        Log.v(DoorManService.TAG, "Uploading file");
        InetAddress address = null;
        try {
            address = InetAddress.getByName("tid.system-ns.net");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        Log.v(DoorManService.TAG,address.toString());
        Socket client = null;
        try {
            client = new Socket(address, 6666);

            OutputStream outputStream = client.getOutputStream();
            //Write file name size and file name bytes
            outputStream.write(file.getName().getBytes().length);
            outputStream.write(file.getName().getBytes());
            Log.v(DoorManService.TAG, "Connection established");
            //Send the file
            byte[] mybytearray = new byte[(int) file.length()]; //create a byte array to file

            FileInputStream fileInputStream = new FileInputStream(file);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

            bufferedInputStream.read(mybytearray, 0, mybytearray.length); //read the file

            outputStream.write(mybytearray, 0, mybytearray.length); //write file to the output stream byte by byte
            outputStream.flush();
            bufferedInputStream.close();
            outputStream.close();
            client.close();
            file.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Object();
    }
}
